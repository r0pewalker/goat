# Tame The Testing Goat

Seems like my workplace is gonna bear with me writing tests.
Thought I'd read and code through the supposed bible since it ticks so many boxes

http://www.obeythetestinggoat.com/pages/book.html

When I'm done I'm going to

1. setup the mock app for Docker/Docker Compose
2. Rewrite tests written with Selenium/Web Drivers to use that shiny new headless Chromium
3. Implement a full continuos stack on some PAAS, for kicks.
