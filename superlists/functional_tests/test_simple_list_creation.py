from .base import FunctionalTest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time


class NewVisitorTest(FunctionalTest):

    def test_can_start_a_list_and_retrieve_it_later(self):
        # visitor checks homepage
        self.browser.get(self.server_url)
        # firefox to webdriver version match problems
        time.sleep(3)
        # page title and header mention todo lists
        self.assertIn('To-Do', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('To-Do', header_text)
        # visitor inputs a to-do item
        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter a to-do item'
        )
        inputbox.send_keys('Buy peacock feathers')
        # page is updated with to-do list including item after pressing ENTER
        inputbox.send_keys(Keys.ENTER)
        time.sleep(3)
        usera_url = self.browser.current_url
        # genric url format check
        self.assertRegex(usera_url, r'/lists/.+')
        self.check_for_row_in_list_table('1: Buy peacock feathers')
        # The text box is used to add another item.
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('Use peacock feathers to make a fly')
        inputbox.send_keys(Keys.ENTER)
        time.sleep(3)
        # The page updates again, and now shows both items
        self.check_for_row_in_list_table('1: Buy peacock feathers')
        self.check_for_row_in_list_table(
            '2: Use peacock feathers to make a fly'
        )
        # another user accesses the homepage
        # a new browser session is needed to avoid cookie carryover
        self.browser.quit()
        self.browser = webdriver.Firefox()
        self.browser.get(self.server_url)
        time.sleep(3)
        page_text = self.browser.find_element_by_tag_name("body").text
        self.assertNotIn("Buy peacock feathers", page_text)
        inputbox = self.browser.find_element_by_id("id_new_item")
        inputbox.send_keys("Buy milk")
        inputbox.send_keys(Keys.ENTER)
        time.sleep(3)
        page_text = self.browser.find_element_by_tag_name("body").text
        userb_url = self.browser.current_url
        self.assertRegex(userb_url, r'/lists/.+')
        # after standard interaction, retest page status and url collision
        self.assertNotEqual(usera_url, userb_url)
        self.assertNotIn("Buy peacock feathers", page_text)
        self.assertIn("Buy milk", page_text)
