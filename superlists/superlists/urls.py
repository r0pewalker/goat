from django.conf.urls import patterns, url, include
from lists import views

urlpatterns = patterns('',
                       url(r'^$', 'lists.views.home_page', name='home_page'),
                       url(r'^lists/', include('lists.urls')),
                       )
